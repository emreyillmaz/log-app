//
//  UserTableViewController.h
//  OrantilaApp
//
//  Created by Emre YILMAZ on 03/11/15.
//  Copyright © 2015 Emre YILMAZ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>


@interface UserTableViewController : UITableViewController


@property (nonatomic, strong) IBOutlet UITableViewCell *theStaticCell;

@property (weak, nonatomic) IBOutlet UILabel *uILabelBaslik;


@end
