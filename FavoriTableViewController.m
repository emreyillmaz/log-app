//
//  FavoriTableViewController.m
//  OrantilaApp
//
//  Created by Emre YILMAZ on 03/11/15.
//  Copyright © 2015 Emre YILMAZ. All rights reserved.
//

#import "FavoriTableViewController.h"
#import "LogTableViewCell.h"
#import "GunlukEntity.h"
#import "DayManager.h"
#import "NewArticleViewController.h"
#import "ETGlobal.h"

@interface FavoriTableViewController ()<LogTableViewCellDelegate,NewArticleViewControllerDelegate>

@property (nonatomic, assign) NSInteger objectCount;
@property (nonatomic) NSMutableArray * logs ;
@property (nonatomic) GunlukEntity *logPro;

-(void)kullaniciSorgula;

@end

@implementation FavoriTableViewController
@synthesize objectCount,logs,logPro;


#pragma mark - Oublic Methods

-(void) refresh{
    // TODO: yeni query
    [self kullaniciSorgula];
    NSLog(@"FavoriTableViewController ready");
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBar.topItem.title = TEXT(@"Title_Favorite");
    
    if ([PFUser currentUser]) {
        [self kullaniciSorgula];
    }
    
    [PFUser enableRevocableSessionInBackground];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(void)viewDidAppear:(BOOL)animated {
    if ([PFUser currentUser]) {
        [self kullaniciSorgula];
    }
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return logs.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   LogTableViewCell*cell = [tableView dequeueReusableCellWithIdentifier:@"favoriCell" forIndexPath:indexPath];
    
    [cell setDelegate:self];
    
  
    
    NSInteger cellNumber = indexPath.row;
    
    GunlukEntity * log =logs[cellNumber];
    //[cell.uILabelGunYazi setText:((GunlukEntity*)logs[cellNumber]).baslik];
    [cell.uILabelIcerik setText:log.icerik];
    [cell.uILabelBaslik setText:log.baslik];
    [cell.uILabelGun setText:[GunlukEntity gun:log.date]];
    [cell.uILabelAy setText:[GunlukEntity ay:log.date]];
    [cell.uILabelGunYazi setText:[GunlukEntity gunYazi:log.date]];
    [cell.uILabelSaat setText:[GunlukEntity saat:log.date]];
    [cell.uILabelYil setText:[GunlukEntity yıl:log.date]];
    [cell.uIButtonFavori setSelected:log.favorimi];
    cell.log = logs[cellNumber];
    
    // Configure the cell...
    
    return cell;
}

#pragma mark - Edit
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //[tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationLeft];
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        if (tableView != self.searchDisplayController.searchResultsTableView){
            
            GunlukEntity * log =logs[indexPath.row];
            [logs removeObjectAtIndex:indexPath.row];
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [log deleteInBackground];
            
        }       //[tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
    }
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // NSLog(@"Sender Id : %@",sender);
    
    if ([sender isKindOfClass:[UITableViewCell class]]) {
        
        GunlukEntity * log =logs[self.tableView.indexPathForSelectedRow.row];
        [((NewArticleViewController*)segue.destinationViewController) setLog:log];
        // NSLog(@" list %@",log.objectId);
        
    }
 
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}
-(void)kullaniciSorgula{
    
    PFQuery *query = [PFQuery queryWithClassName:@"GunlukEntity"];
    [query whereKey:@"author" equalTo:[PFUser currentUser]];
    [query whereKey:@"favorimi" equalTo:@YES];
    [query orderByDescending:@"createdAt"];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            // The find succeeded.
            NSLog(@"Successfully favorite retrieved %lu scores.", (unsigned long)objects.count);
            // Do something with the found objects
            
            objectCount = objects.count;
            logs= [NSMutableArray arrayWithArray:objects];
            
            [self.tableView reloadData];
        } else {
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
    }];
}

#pragma mark - LogTableViewCellDelegate Methods

-(void)unFavTouchUpInside:(id)sender{
    NSLog(@"favori değişti.");
    [self kullaniciSorgula];
    
}
#pragma mark -NewArticleAddDelegate Methods

-(void)saveTouchUpInside:(id)sender{
    
    [self kullaniciSorgula];
    NSLog(@"ana ekran skaydetme calisti");
}

@end
