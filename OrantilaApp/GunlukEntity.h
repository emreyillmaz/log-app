//
//  GunlukEntity.h
//  OrantilaApp
//
//  Created by Emre YILMAZ on 26/10/15.
//  Copyright © 2015 Emre YILMAZ. All rights reserved.
//

#import <Parse/Parse.h>

@interface GunlukEntity : PFObject<PFSubclassing>

+ (NSString *)parseClassName;

@property (nonatomic, strong) NSString *baslik;
@property (nonatomic, strong) NSString *icerik;
@property (nonatomic, strong) NSString *author;
@property (nonatomic) BOOL *isDraft;
@property (nonatomic, strong,setter=setUuidString:) NSString *uuidString;

@property (nonatomic, strong) NSDate * date;
@property (nonatomic) BOOL favorimi;

+(NSString *)gun:(NSDate *)date;
+(NSString *)gunYazi:(NSDate *)date;
+(NSString *)ay:(NSDate *)date;
+(NSString *)yıl:(NSDate *)date;
+(NSString *)saat:(NSDate *)date;
@end
