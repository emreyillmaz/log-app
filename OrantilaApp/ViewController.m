//
//  ViewController.m
//  OrantilaApp
//
//  Created by Emre YILMAZ on 20/10/15.
//  Copyright © 2015 Emre YILMAZ. All rights reserved.
//

#import "ViewController.h"
#import "ETGlobal.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UITextField *birTextField;
@property (weak, nonatomic) IBOutlet UITextField *ikiTextField;
@property (weak, nonatomic) IBOutlet UITextField *ucTextField;
@property (weak, nonatomic) IBOutlet UILabel *sonucLabel;
@property (weak, nonatomic) IBOutlet UISegmentedControl *belirleyici;

@property (nonatomic, assign) NSInteger birinciSayi;
@property (nonatomic, assign) NSInteger ikinciSayi;
@property (nonatomic, assign) NSInteger ucuncuSayi;
@property (nonatomic, assign) NSInteger sonucSayi;
@property (nonatomic, assign) NSInteger newValueInt;

@end

@implementation ViewController
@synthesize birinciSayi, ikinciSayi, ucuncuSayi,sonucLabel,sonucSayi;

@synthesize birTextField,ikiTextField,ucTextField;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    birTextField.keyboardType = UIKeyboardTypeNumberPad;
    self.ikiTextField.keyboardType = UIKeyboardTypeNumberPad;
    self.ucTextField.keyboardType  = UIKeyboardTypeNumberPad;
    
   
    NSString* dilMetni = NSLocalizedString(@"Dil_metini", nil);
    dilMetni = TEXT(@"Dil_metini");
}

 - (IBAction)SegmentedCtrlDurumu:(id)sender;{
    
    }



- (IBAction)hesaplaBasildi:(id)sender {
    
    PFObject *testObject = [PFObject objectWithClassName:@"TestObject"];
    testObject[@"deneme"] = @"bar";
    [testObject saveInBackground];
    
    self.birinciSayi = [self.birTextField.text integerValue];
    self.ikinciSayi = [self.ikiTextField.text integerValue];
    self.ucuncuSayi = [self.ucTextField.text integerValue];
    NSInteger tempSonuc=0;
    if (self.belirleyici.selectedSegmentIndex == 1) {
        
        tempSonuc=(self.ucuncuSayi * self.ikinciSayi) / self.birinciSayi;
        NSString *myString = [@(tempSonuc) stringValue];
        [sonucLabel setText:myString];
    }
    else {
    
        tempSonuc = (self.birinciSayi * self.ikinciSayi) / self.ucuncuSayi;
        NSString *myString = [@(tempSonuc) stringValue];
        [sonucLabel setText:myString];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}



@end
