//
//  AnaEkranTableViewController.m
//  OrantilaApp
//
//  Created by Emre YILMAZ on 27/10/15.
//  Copyright © 2015 Emre YILMAZ. All rights reserved.
//

#import "AnaEkranTableViewController.h"
#import "LogTableViewCell.h"
#import "GunlukEntity.h"
#import "DayManager.h"
#import "NewArticleViewController.h"
#import "ETGlobal.h"

@interface AnaEkranTableViewController ()<UISearchBarDelegate, UISearchDisplayDelegate>{
}


@property (nonatomic, assign) NSInteger objectCount;
@property (nonatomic) NSMutableArray * logs ;
@property (nonatomic) NSMutableArray * results;
@property (nonatomic) GunlukEntity *logPro;
-(void)kullaniciSorgula;

@end

@implementation AnaEkranTableViewController
@synthesize objectCount,logs,logPro,results;

#pragma mark - Oublic Methods

-(void) refresh{
    // TODO: yeni query
    [self kullaniciSorgula];
    NSLog(@"AnaEkranTableViewController ready");
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [PFUser enableRevocableSessionInBackground];
   
        self.navigationController.navigationBar.topItem.title = TEXT(@"Title_Log");
   
    }

-(void)viewWillAppear:(BOOL)animated{
    
    if ([PFUser currentUser]) {
        [self kullaniciSorgula];
    }
}



-(void)viewDidAppear:(BOOL)animated{
  
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//#pragma mark Content Filtering
//-(void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope {
//    // Update the filtered array based on the search text and scope.
//    // Remove all objects from the filtered search array
//    [self.filteredArananEntityArray removeAllObjects];
//    // Filter the array using NSPredicate
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.name contains[c] %@",searchText];
//    self.filteredArananEntityArray = [NSMutableArray arrayWithArray:[logs filteredArrayUsingPredicate:predicate]];
//}


#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (tableView == self.tableView) {
        

        return logs.count;
        
    } else {
        
        return self.results.count;
        
    }
    
    
 //[gunlukEntity setObject:[PFUser currentUser] forKey:@"author"];
    
  //  NSLog(@"objectCount %ld",objectCount);
   // return objectCount;
}

//#pragma mark - UISearchDisplayController Delegate Methods
//-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
//    // Tells the table data source to reload when text changes
//    [self filterContentForSearchText:searchString scope:
//     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
//    // Return YES to cause the search result table view to be reloaded.
//    return YES;
//}
//
//-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption {
//    // Tells the table data source to reload when scope bar selection changes
//    [self filterContentForSearchText:self.searchDisplayController.searchBar.text scope:
//     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:searchOption]];
//    // Return YES to cause the search result table view to be reloaded.
//    return YES;
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    LogTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"entityIdfCell" forIndexPath:indexPath];
    NSInteger cellNumber = indexPath.row;
    //GunlukEntity * log =logs[cellNumber];
    
//   
//    if (!cell) {
//        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"LogTableViewCell" owner:nil options:nil];
//        
//        for (id currentObject in topLevelObjects)
//        {
//            if([currentObject isKindOfClass:[LogTableViewCell class]])
//            {
//                cell = (LogTableViewCell *)currentObject;
//                break;
//            }
//        }
//    }

    if (tableView != self.searchDisplayController.searchResultsTableView){
        GunlukEntity * log =logs[cellNumber];
        [cell.uILabelIcerik setText:log.icerik];
        [cell.uILabelBaslik setText:log.baslik];
        [cell.uILabelGun setText:[GunlukEntity gun:log.date]];
        [cell.uILabelAy setText:[GunlukEntity ay:log.date]];
        [cell.uILabelGunYazi setText:[GunlukEntity gunYazi:log.date]];
        [cell.uILabelSaat setText:[GunlukEntity saat:log.date]];
        [cell.uILabelYil setText:[GunlukEntity yıl:log.date]];
        [cell.uIButtonFavori setSelected:log.favorimi];
        cell.log = logs[cellNumber];
        
    }else {
        GunlukEntity * result =results[cellNumber];
        [cell.uILabelIcerik setText:result.icerik];
        [cell.uILabelBaslik setText:result.baslik];
        [cell.uILabelGun setText:[GunlukEntity gun:result.date]];
        [cell.uILabelAy setText:[GunlukEntity ay:result.date]];
        [cell.uILabelGunYazi setText:[GunlukEntity gunYazi:result.date]];
        [cell.uILabelSaat setText:[GunlukEntity saat:result.date]];
        [cell.uILabelYil setText:[GunlukEntity yıl:result.date]];
        [cell.uIButtonFavori setSelected:result.favorimi];
        cell.log = results[cellNumber];
  
    }
    
    //[cell.uILabelGunYazi setText:((GunlukEntity*)logs[cellNumber]).baslik];
        // Configure the cell...
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

#pragma mark - Edit
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //[tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationLeft];

    if (editingStyle == UITableViewCellEditingStyleDelete) {
        if (tableView != self.searchDisplayController.searchResultsTableView){
   
            GunlukEntity * log =logs[indexPath.row];
            [logs removeObjectAtIndex:indexPath.row];
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [log deleteInBackground];
        
        }else {
            GunlukEntity * log =results[indexPath.row];
            [results removeObjectAtIndex:indexPath.row];
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            
         
            //GunlukEntity * log2 =logs[indexPath.row];
            //[logs removeObjectAtIndex:indexPath.row];
            //[tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [log deleteInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
                if (!error) {
                    [self kullaniciSorgula];
                }
            }];
           
        
        }
        
        
        
        
        //[tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    
    }


}





-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
     if ([sender isKindOfClass:[UITableViewCell class]]) {
        
        GunlukEntity * log =logs[self.tableView.indexPathForSelectedRow.row];
        [((NewArticleViewController*)segue.destinationViewController) setLog:log];
       // NSLog(@" list %@",log.objectId);
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 207;
}


#pragma mark - Fonsiyonlar

-(void)kullaniciSorgula{
   
    PFQuery *query = [PFQuery queryWithClassName:@"GunlukEntity"];
    
    [query orderByDescending:@"createdAt"];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            // The find succeeded.
            NSLog(@"Successfully retrieved %lu scores.", (unsigned long)objects.count);
            // Do something with the found objects
            
            objectCount = objects.count;
            logs= [NSMutableArray arrayWithArray:objects];
            
            [self.tableView reloadData];
        } else {
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
    }];
}
-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
    [self makeSearch:searchString];
    return YES;
}
-(void)makeSearch:(NSString*)text{
   
    if (text.length > 0) {
        NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"SELF.icerik contains[cd] %@", text];
        results = [NSMutableArray arrayWithArray:[logs filteredArrayUsingPredicate:bPredicate]];
        
       // NSLog(@"%lu",(unsigned long)results.count);
    }else
    {
        results = [NSMutableArray arrayWithArray:logs];
        text = @"";
      
    }
}
@end
