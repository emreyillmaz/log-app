//
//  NewArticleViewController.m
//  OrantilaApp
//
//  Created by Emre YILMAZ on 26/10/15.
//  Copyright © 2015 Emre YILMAZ. All rights reserved.
//

#import "NewArticleViewController.h"
#import "GunlukEntity.h"
#import "AnaEkranTableViewController.h"

@interface NewArticleViewController (){
    GunlukEntity *gunlukEntity;
   
    NSString *gunlukEntityId ;
    NSInteger kontrol;
}

@end

@implementation NewArticleViewController
@synthesize uILabelAy,uILabelGun,uILabelGunYazi,uILabelSaat,uILabelYil,uITextFieldBaslik,uITextFieldIcerik,log;



#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
   /*
    [PFUser logInWithUsernameInBackground:@"emre4" password:@"4" block:^(PFUser *user, NSError *error) {
        if (user) {
            // Do stuff after successful login.
            NSLog(@"User Logged In");
            
        } else {
            // The login failed. Check error to see why.
            NSLog(@"User Login failed");"
        }
    }];
    */
    
    //gunlukEntityId = @"nil";
    
    
 
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    
    
    [uITextFieldIcerik becomeFirstResponder];
    // NSLog(@"gunlukEntityId %@",gunlukEntityId);
    
    //  NSLog(@"detail %@",log.objectId);
    gunlukEntityId = log.objectId;
    
    if (gunlukEntityId == nil) {
        kontrol = 100;
        NSDate *today = [NSDate date];
        //        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        //        // display in 12HR/24HR (i.e. 11:25PM or 23:25) format according to User Settings
        //        [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
        //
        //        NSCalendar *calendar = [[NSCalendar alloc]initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        //
        //        NSCalendarUnit units = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitWeekday ;
        //        NSDateComponents *components = [calendar components:units fromDate:today];
        //
        //        NSString *monthName = [[dateFormatter monthSymbols] objectAtIndex:([components month]-1)];
        //        NSString *weekDayName = [[dateFormatter weekdaySymbols] objectAtIndex:([components weekday]-1)];
        //        NSString *currentTime = [dateFormatter stringFromDate:today];
        //
        //        //        NSLog(@"User's current time in their preference format:%@",currentTime);
        //        //        NSLog(@"Day: %ld", [components day]);
        //        //        NSLog(@"Month: %ld", [components month]);
        //        //        NSLog(@"Year: %ld", [components year]);
        //        //        NSLog(@"WeekDay:%ld", [components weekday]);
        //        //        NSLog(@"%@ %@", monthName,weekDayName);
        
        uILabelAy.text = [GunlukEntity ay:today];
        uILabelGun.text =[GunlukEntity gun:today];
        uILabelGunYazi.text = [GunlukEntity gunYazi:today];
        uILabelSaat.text = [GunlukEntity saat:today];
        uILabelYil.text = [GunlukEntity yıl:today];
       
        
        //        uILabelGun.text = [NSString stringWithFormat:@"%ld", (long)[components day]];
        //        [uILabelGunYazi setText:weekDayName];
        //        [uILabelAy setText:monthName];
        //        uILabelYil.text = [NSString stringWithFormat:@"%ld", (long)[components year]];
        //        [uILabelSaat setText:currentTime];
        
        gunlukEntity = [[GunlukEntity alloc] init];
        [gunlukEntity setObject:[PFUser currentUser] forKey:@"author"];
        
    }
    else {
        kontrol = 200;
        gunlukEntity = log;
        uITextFieldBaslik.text = log.baslik;
        uITextFieldIcerik.text = log.icerik;
        uILabelAy.text = [GunlukEntity ay:log.date];
        uILabelGun.text =[GunlukEntity gun:log.date];
        uILabelGunYazi.text = [GunlukEntity gunYazi:log.date];
        uILabelSaat.text = [GunlukEntity saat:log.date];
        uILabelYil.text = [GunlukEntity yıl:log.date];
        [self.uIButtonFavori setSelected:[gunlukEntity favorimi]];
        
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Button Events

-(IBAction)btnFavToucUpInside:(id)sender{
    if (self.uIButtonFavori.isSelected) {
        [self.uIButtonFavori setSelected:NO];
        [self favCikar];
    }else{
        [self.uIButtonFavori setSelected:YES];
        [self favEkle];
    }
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)actionArticleSaveClick:(id)sender {
    
    [self baslikVeIcerikEkle];
    //[self.kaydetDekegate saveTouchUpInside:self];
    
    [self.delegate saveTouchUpInside:self];
   
    gunlukEntity[@"isDraft"] = @NO;
    gunlukEntity.ACL= [PFACL ACLWithUser:[PFUser currentUser]];
    if (kontrol == 100) {
         [self tarihEkle];
        gunlukEntity[@"uuid"]=[self uniqueFileName];
    }
    [gunlukEntity saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
        if (!error) {
            [self.navigationController popViewControllerAnimated:YES];
            
        }
        else {
         NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
    }];
   
    //[self dismissViewControllerAnimated:YES completion:nil];
    
    
}


-(void)baslikVeIcerikEkle{

    NSString *trimmedBaslik = [uITextFieldBaslik.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *trimmedIcerik = [uITextFieldIcerik.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    gunlukEntity[@"baslik"] = trimmedBaslik;
    gunlukEntity[@"icerik"] = trimmedIcerik;
    
}

#pragma mark - Private Methods

-(void)tarihEkle {
     NSDate *today = [NSDate date];
    gunlukEntity[@"date"]= today;
}

- (NSString *)uniqueFileName
{
    CFUUIDRef theUniqueString = CFUUIDCreate(NULL);
    CFStringRef string = CFUUIDCreateString(NULL, theUniqueString);
    CFRelease(theUniqueString);
    
    return (__bridge NSString *)(string);
}

-(void)favEkle{
    gunlukEntity[@"favorimi"]=@YES;
    [gunlukEntity saveInBackground];
}

-(void)favCikar {
    gunlukEntity[@"favorimi"]=@NO;
    [gunlukEntity saveInBackground];
}

@end
