//
//  NewArticleViewController.h
//  OrantilaApp
//
//  Created by Emre YILMAZ on 26/10/15.
//  Copyright © 2015 Emre YILMAZ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "GunlukEntity.h"


@protocol NewArticleViewControllerDelegate <NSObject>

@required
-(void)saveTouchUpInside:(id)sender;

@end

@interface NewArticleViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *uITextFieldBaslik;
@property (weak, nonatomic) IBOutlet UITextView *uITextFieldIcerik;

@property (weak, nonatomic) IBOutlet UILabel *uILabelGun;
@property (weak, nonatomic) IBOutlet UILabel *uILabelGunYazi;
@property (weak, nonatomic) IBOutlet UILabel *uILabelAy;
@property (weak, nonatomic) IBOutlet UILabel *uILabelYil;
@property (weak, nonatomic) IBOutlet UILabel *uILabelSaat;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *uIBarButtonItemEkle;

@property (weak,nonatomic)GunlukEntity *log;

@property (weak, nonatomic) IBOutlet UIButton *uIButtonFavori;

@property (nonatomic)id<NewArticleViewControllerDelegate> delegate;

@end
