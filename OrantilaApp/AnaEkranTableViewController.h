//
//  AnaEkranTableViewController.h
//  OrantilaApp
//
//  Created by Emre YILMAZ on 27/10/15.
//  Copyright © 2015 Emre YILMAZ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>


@interface AnaEkranTableViewController : UITableViewController
-(void)kullaniciSorgula;

-(void) refresh;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) UISearchController *searchController;
@property (strong,nonatomic) NSMutableArray *searchResults;

@end
    