//
//  SaatlerViewController.h
//  OrantilaApp
//
//  Created by Emre YILMAZ on 24/10/15.
//  Copyright © 2015 Emre YILMAZ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SaatlerViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *myTableView;

@property (nonatomic)NSInteger dayIndex;

@end
