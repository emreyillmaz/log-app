//
//  DayManager.h
//  OrantilaApp
//
//  Created by Emre YILMAZ on 24/10/15.
//  Copyright © 2015 Emre YILMAZ. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DayManager : NSObject

+(NSString *)gunGonder:(NSInteger)gelenIndex;
+(NSInteger)dayCount;
+(NSString *)gun:(NSDate *)date;
+(NSString *)gunYazi:(NSDate *)date;
+(NSString *)ay:(NSDate *)date;

@end
