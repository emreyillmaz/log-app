//
//  LogTableViewCell.m
//  OrantilaApp
//
//  Created by Emre YILMAZ on 27/10/15.
//  Copyright © 2015 Emre YILMAZ. All rights reserved.
//

#import "LogTableViewCell.h"


@implementation LogTableViewCell
@synthesize uILabelAy,uILabelBaslik,uILabelGun,uILabelGunYazi,uILabelIcerik,uILabelSaat,uILabelYil,log;

- (void)awakeFromNib {
  

    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(IBAction)btnFav:(id)sender{
    
    if (self.uIButtonFavori.isSelected) {
        [self.uIButtonFavori setSelected:NO];
       // [self favCikar];
        log[@"favorimi"]=@NO;
        [log saveInBackground];
        
    }else{
        [self.uIButtonFavori setSelected:YES];
        //[self favEkle];
        log[@"favorimi"]=@YES;
        [log saveInBackground];
    }
    
    [self.delegate unFavTouchUpInside:self];
}
@end
