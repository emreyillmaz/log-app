//
//  LoginViewController.m
//  OrantilaApp
//
//  Created by Emre YILMAZ on 30/10/15.
//  Copyright © 2015 Emre YILMAZ. All rights reserved.
//

#import "LoginViewController.h"



@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
        
//    // Do any additional setup after loading the view.
//    
  // @property (PFUI_NULLABLE_PROPERTY nonatomic, strong) UIView *logo;
    PFLogInViewController * login = [[PFLogInViewController alloc] init];
    [self presentViewController:login animated:YES completion:nil ];

    [login.logInView setLogo:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo_logo.png"]]];
    
    //[login.logInView setLogo:<#(UIView * _Nullable)#>]
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated{
    PFLogInViewController * login = [[PFLogInViewController alloc] init];
    [self presentViewController:login animated:YES completion:nil ];


}
- (void)logInViewController:(PFLogInViewController *)logInController didLogInUser:(PFUser *)user {
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"customMainNav"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:vc animated:YES completion:NULL];
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
