//
//  GunlukEntity.m
//  OrantilaApp
//
//  Created by Emre YILMAZ on 26/10/15.
//  Copyright © 2015 Emre YILMAZ. All rights reserved.
//

#import "GunlukEntity.h"
#import <Parse/PFObject+Subclass.h>


@implementation GunlukEntity
@dynamic baslik,author,date,isDraft,favorimi,uuidString,icerik;

+ (void)load {
    [self registerSubclass];
}

+ (NSString *)parseClassName {
    return @"GunlukEntity";
}


- (NSString *)UuidString {
    // Returns a UUID
    
    CFUUIDRef uuid = CFUUIDCreate(kCFAllocatorDefault);
    NSString *uuidString = (__bridge_transfer NSString *)CFUUIDCreateString(kCFAllocatorDefault, uuid);
    CFRelease(uuid);
    
    return uuidString;
}

+(NSString *)gun:(NSDate *)date{
    
    NSCalendar *calendar = [[NSCalendar alloc]initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSCalendarUnit units =  NSCalendarUnitDay | NSCalendarUnitWeekday ;
    NSDateComponents *components = [calendar components:units fromDate:date];
    
    return [NSString stringWithFormat:@"%ld", (long)[components day]];
}

+(NSString *)gunYazi:(NSDate *)date;{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSCalendar *calendar = [[NSCalendar alloc]initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSCalendarUnit units =NSCalendarUnitDay | NSCalendarUnitWeekday ;
    NSDateComponents *components = [calendar components:units fromDate:date];
    NSString *weekDayName = [[dateFormatter weekdaySymbols] objectAtIndex:([components weekday]-1)];
    
    return weekDayName ;
}
+(NSString *)ay:(NSDate *)date{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSCalendar *calendar = [[NSCalendar alloc]initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSCalendarUnit units =NSCalendarUnitMonth ;
    NSDateComponents *components = [calendar components:units fromDate:date];
    NSString *monthName = [[dateFormatter monthSymbols] objectAtIndex:([components month]-1)];
    return monthName;
}

+(NSString *)yıl:(NSDate *)date {
    
    NSCalendar *calendar = [[NSCalendar alloc]initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSCalendarUnit units = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitWeekday ;
    NSDateComponents *components = [calendar components:units fromDate:date];

    
    return [NSString stringWithFormat:@"%ld", (long)[components year]];
}
+(NSString *)saat:(NSDate *)date{
  
     NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        //[dateFormatter setTimeStyle:NSDateFormatterShortStyle];
    [dateFormatter setDateFormat:@"HH:mm"];
    NSString *currentTime = [dateFormatter stringFromDate:date];
    return currentTime;
}

@end
