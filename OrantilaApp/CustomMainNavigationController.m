//
//  CustomMainNavigationController.m
//  OrantilaApp
//
//  Created by Emre YILMAZ on 30/10/15.
//  Copyright © 2015 Emre YILMAZ. All rights reserved.
//

#import "CustomMainNavigationController.h"
#import "AnaEkranTableViewController.h"
#import "FavoriTableViewController.h"
#import "ETGlobal.h"

@interface CustomMainNavigationController()<PFLogInViewControllerDelegate,PFSignUpViewControllerDelegate>{
    
}

@end
@implementation CustomMainNavigationController


-(void)viewDidLoad {
    [super viewDidLoad];
   
   // AnaEkranTableViewController *navCon  = ( AnaEkranTableViewController*) [self.];
    //navCon.navigationItem.title = @"Hello";

    NSLog(@"nav cont. : %@", self.navigationController.viewControllers );
    
    
}

-(void)viewWillAppear:(BOOL)animated{
  
    
    //[self.viewControllers.lastObject presentViewController:login animated:YES completion:nil];
}

-(void)viewDidAppear:(BOOL)animated {
    if (![PFUser currentUser]) {
        PFLogInViewController * login = [[PFLogInViewController alloc] init];
      
        
        [login.logInView setLogo:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"log_logo"]]];
        [login.logInView setBackgroundColor:[UIColor colorWithRed:0.47 green:0.13 blue:0.44 alpha:1.0] ];
        
        
       // [login.logInView.passwordForgottenButton setBackgroundColor:[UIColor colorWithRed:0.55 green:0.30 blue:0.52 alpha:1.0]];
        //[login.logInView.passwordForgottenButton setTitle:@"TESTING" forState:UIControlStateNormal];
        [login.logInView.passwordForgottenButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [login.logInView.signUpButton setBackgroundImage:nil forState:UIControlStateNormal];
        [login.logInView.signUpButton setBackgroundImage:nil forState:UIControlStateHighlighted];
        [login.logInView.signUpButton setBackgroundColor:[UIColor colorWithRed:0.55 green:0.30 blue:0.52 alpha:1.0]];
        [login.logInView.signUpButton setTitle:@"" forState:UIControlStateNormal];
        [login.logInView.signUpButton setTitle:@"" forState:UIControlStateHighlighted];
        [login.logInView.logInButton setBackgroundImage:nil forState:UIControlStateNormal ];
        [login.logInView.logInButton setBackgroundImage:nil forState:UIControlStateHighlighted ];
        [login.logInView.logInButton setBackgroundColor:[UIColor colorWithRed:0.55 green:0.30 blue:0.52 alpha:1.0]];
        [login.logInView.dismissButton setHidden:YES];
        [login.logInView.dismissButton setBackgroundImage:nil forState:UIControlStateNormal];
        [login.logInView.dismissButton setBackgroundImage:nil forState:UIControlStateHighlighted];
        [login.logInView.dismissButton setBackgroundColor:[UIColor colorWithRed:0.47 green:0.13 blue:0.44 alpha:1.0]];
        [login.logInView.usernameField setTextColor:[UIColor whiteColor]];
        [login.logInView.usernameField setSeparatorColor:[UIColor colorWithRed:0.47 green:0.13 blue:0.44 alpha:1.0]];
        [login.logInView.usernameField setBackgroundColor:[UIColor colorWithRed:0.55 green:0.30 blue:0.52 alpha:1.0]];
        [login.logInView.passwordField setTextColor:[UIColor whiteColor]];
        [login.logInView.passwordField setBackgroundColor:[UIColor colorWithRed:0.55 green:0.30 blue:0.52 alpha:1.0]];
        [login.logInView.passwordField setSeparatorColor:[UIColor colorWithRed:0.47 green:0.13 blue:0.44 alpha:1.0]];
        
        
        PFSignUpViewController * signUp =[[PFSignUpViewController alloc]init];
        [signUp setDelegate:self];
        [signUp setFields:PFSignUpFieldsDefault | PFSignUpFieldsAdditional];
        
         [login setSignUpController:signUp];
        //[login setSignUpController:self];
        
        [signUp.signUpView setLogo:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"log_logo"]]];
        [signUp.signUpView setBackgroundColor:[UIColor colorWithRed:0.47 green:0.13 blue:0.44 alpha:1.0]];
        [signUp.signUpView.signUpButton setBackgroundImage:nil forState:UIControlStateNormal];
        [signUp.signUpView.signUpButton setBackgroundImage:nil forState:UIControlStateHighlighted];
        [signUp.signUpView.signUpButton setBackgroundColor:[UIColor colorWithRed:0.55 green:0.30 blue:0.52 alpha:1.0]];
        [signUp.signUpView.usernameField setBackgroundColor:[UIColor colorWithRed:0.55 green:0.30 blue:0.52 alpha:1.0]];
        [signUp.signUpView.usernameField setTextColor:[UIColor whiteColor]];
        [signUp.signUpView.usernameField setSeparatorColor:[UIColor colorWithRed:0.47 green:0.13 blue:0.44 alpha:1.0]];
        [signUp.signUpView.passwordField setBackgroundColor:[UIColor colorWithRed:0.55 green:0.30 blue:0.52 alpha:1.0]];
        [signUp.signUpView.passwordField setTextColor:[UIColor whiteColor]];
        [signUp.signUpView.passwordField setSeparatorColor:[UIColor colorWithRed:0.47 green:0.13 blue:0.44 alpha:1.0]];
        [signUp.signUpView.emailField setBackgroundColor:[UIColor colorWithRed:0.55 green:0.30 blue:0.52 alpha:1.0]];
        [signUp.signUpView.emailField setTextColor:[UIColor whiteColor]];
        [signUp.signUpView.emailField setSeparatorColor:[UIColor colorWithRed:0.47 green:0.13 blue:0.44 alpha:1.0]];
        [signUp.signUpView.additionalField setBackgroundColor:[UIColor colorWithRed:0.55 green:0.30 blue:0.52 alpha:1.0]];
        [signUp.signUpView.additionalField setTextColor:[UIColor whiteColor]];
        [signUp.signUpView.additionalField setSeparatorColor:[UIColor colorWithRed:0.47 green:0.13 blue:0.44 alpha:1.0]];
        
       
        //[signUp.signUpView.dismissButton setBackgroundColor:[UIColor whiteColor]];
        
        
        [login setDelegate:self];
        [self presentViewController:login animated:NO completion:nil ];
        [self presentViewController:signUp animated:NO completion:nil ];
        
    }
    
}

- (void)logInViewController:(PFLogInViewController *)logInController didLogInUser:(PFUser *)user {
    [logInController dismissViewControllerAnimated:YES completion:nil];
    AnaEkranTableViewController *d=(AnaEkranTableViewController*)[self topViewController];
    FavoriTableViewController * e= (FavoriTableViewController*)[self topViewController];

    [e kullaniciSorgula];
    [d kullaniciSorgula];
    
}



@end

