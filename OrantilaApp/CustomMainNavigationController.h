//
//  CustomMainNavigationController.h
//  OrantilaApp
//
//  Created by Emre YILMAZ on 30/10/15.
//  Copyright © 2015 Emre YILMAZ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import <ParseUI/ParseUI.h>

@interface CustomMainNavigationController : UINavigationController

@end
