//
//  LogTabbarViewController.m
//  OrantilaApp
//
//  Created by Emre YILMAZ on 04/11/15.
//  Copyright © 2015 Emre YILMAZ. All rights reserved.
//

#import "LogTabbarViewController.h"
#import "FavoriTableViewController.h"
#import "AnaEkranTableViewController.h"
#import "UserTableViewController.h"
#import "CustomMainNavigationController.h"
#import "ETGlobal.h"

@interface LogTabbarViewController ()<UITabBarControllerDelegate>{

    

}
@property (nonatomic)  NSUInteger kontrol;
@end

@implementation LogTabbarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setDelegate:self];
   
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];

    [[self.tabBar.items objectAtIndex:0] setTitle:TEXT(@"TabBar_Log")];
   // [[self.tabBar.items objectAtIndex:1] setTitle:TEXT(@"TabBar_Favorite")]; //Erişim Sağlamıyor kendi otomatik içeriği.
    [[self.tabBar.items objectAtIndex:2] setTitle:TEXT(@"TabBar_User")];
}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController{
    NSLog(@"didSelect cv :%@ index: %ld", [viewController description], tabBarController.selectedIndex);
    
    
    CustomMainNavigationController * nvc = (CustomMainNavigationController*)viewController;
    
    if ([nvc.topViewController isKindOfClass:[AnaEkranTableViewController class]] && self.kontrol !=0) {
        AnaEkranTableViewController * vc = (AnaEkranTableViewController*)nvc.topViewController;
        // TODO: AnaEkranTableViewController pulic yenileme fonsiyonu çağır.
        [vc refresh];
    }else if ([nvc.topViewController isKindOfClass:[FavoriTableViewController class]]&& self.kontrol !=1){
        FavoriTableViewController * vc = (FavoriTableViewController*)nvc.topViewController;
        // TODO: AnaEkranTableViewController pulic yenileme fonsiyonu çağır.
        [vc refresh];
    }
    
    self.kontrol= tabBarController.selectedIndex;
    
}

@end
