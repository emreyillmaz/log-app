//
//  LogTableViewCell.h
//  OrantilaApp
//
//  Created by Emre YILMAZ on 27/10/15.
//  Copyright © 2015 Emre YILMAZ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GunlukEntity.h"

@protocol LogTableViewCellDelegate <NSObject>

@required
-(void)unFavTouchUpInside:(id)sender;

@end

@interface LogTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *uILabelGun;
@property (weak, nonatomic) IBOutlet UILabel *uILabelGunYazi;
@property (weak, nonatomic) IBOutlet UILabel *uILabelAy;
@property (weak, nonatomic) IBOutlet UILabel *uILabelYil;
@property (weak, nonatomic) IBOutlet UILabel *uILabelBaslik;
@property (weak, nonatomic) IBOutlet UILabel *uILabelIcerik;
@property (weak, nonatomic) IBOutlet UILabel *uILabelSaat;
@property (weak, nonatomic) IBOutlet UIButton *uIButtonFavori;
@property (weak,nonatomic)GunlukEntity *log;
@property (nonatomic) id<LogTableViewCellDelegate> delegate;

@end
