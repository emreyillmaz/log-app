//
//  SaatlerViewController.m
//  OrantilaApp
//
//  Created by Emre YILMAZ on 24/10/15.
//  Copyright © 2015 Emre YILMAZ. All rights reserved.
//

#import "SaatlerViewController.h"
#import "DayManager.h"

@interface SaatlerViewController ()

@end

@implementation SaatlerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    

    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return [DayManager dayCount];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"saatlerCell" forIndexPath:indexPath];


    NSInteger cellNumber = indexPath.row;
    [cell.textLabel setText:[DayManager gunGonder:cellNumber]];
    //cell.textLabel.text = [mainArray objectAtIndex:indexPath.row];
    
    return cell;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
