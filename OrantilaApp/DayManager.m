//
//  DayManager.m
//  OrantilaApp
//
//  Created by Emre YILMAZ on 24/10/15.
//  Copyright © 2015 Emre YILMAZ. All rights reserved.
//

#import "DayManager.h"

@implementation DayManager

+(NSString *)gunGonder:(NSInteger)gelenIndex {
    NSArray *days = [NSArray arrayWithObjects:@"Pazartesi",@"Sali",@"Çarşamba",@"Perşembe",@"Cuma",@"Cumartesi",@"Pazar" ,nil];
    
    return days[gelenIndex];
}

+(NSInteger)dayCount{
    return 7;
}


+(NSString*)mesaiGonder:(NSInteger)gelenIndex {
    
    NSArray *saatler = [NSArray arrayWithObjects:@"8 - 9 ",@"9 - 10",@"10 - 11",nil];
    
    
    return saatler[gelenIndex];
}


+(NSString *)gun:(NSDate*)date{
    
    NSCalendar *calendar = [[NSCalendar alloc]initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSCalendarUnit units =  NSCalendarUnitDay | NSCalendarUnitWeekday ;
    NSDateComponents *components = [calendar components:units fromDate:date];
    
    return [NSString stringWithFormat:@"%ld", (long)[components day]];
}

+(NSString *)gunYazi:(NSDate *)date;{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSCalendar *calendar = [[NSCalendar alloc]initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSCalendarUnit units =NSCalendarUnitDay | NSCalendarUnitWeekday ;
    NSDateComponents *components = [calendar components:units fromDate:date];
    NSString *weekDayName = [[dateFormatter weekdaySymbols] objectAtIndex:([components weekday]-1)];
    
    return weekDayName ;
}
+(NSString *)ay:(NSDate *)date{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSCalendar *calendar = [[NSCalendar alloc]initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSCalendarUnit units =NSCalendarUnitMonth ;
    NSDateComponents *components = [calendar components:units fromDate:date];
    NSString *monthName = [[dateFormatter monthSymbols] objectAtIndex:([components month]-1)];
    return monthName;
}


@end
