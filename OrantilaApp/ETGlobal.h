//
//  ETGlobal.h
//  OrantilaApp
//
//  Created by Emre YILMAZ on 09/11/15.
//  Copyright © 2015 Emre YILMAZ. All rights reserved.
//

#ifndef ETGlobal_h
#define ETGlobal_h

// BU bir makro
#define TEXT(x) NSLocalizedString(x, nil)

#endif /* ETGlobal_h */
