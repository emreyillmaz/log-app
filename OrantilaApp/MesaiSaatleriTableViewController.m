//
//  MesaiSaatleriTableViewController.m
//  OrantilaApp
//
//  Created by Emre YILMAZ on 24/10/15.
//  Copyright © 2015 Emre YILMAZ. All rights reserved.
//

#import "MesaiSaatleriTableViewController.h"
#import "DayManager.h"

@interface MesaiSaatleriTableViewController ()
{
    NSArray *saatler;
    
}

@end

@implementation MesaiSaatleriTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"dayındex %d",self.dayIndex);
    self.navigationItem.title = [DayManager gunGonder:self.dayIndex];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    saatler = [NSArray arrayWithObjects:@"8 - 9",@"9 - 10", @"10 - 11",@"12 - 13",@"ÖGLE ARASI",@"14 - 15",@"15 - 16",@"16 - 17",@"17 - 18" ,nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Incomplete implementation, return the number of sections
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete implementation, return the number of rows
    return [saatler count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"saatlerCell" forIndexPath:indexPath];
    
    // Configure the cell...
    
    NSInteger cellNumber = indexPath.row;
    
    [cell.textLabel setText:saatler[cellNumber]];

    NSLog(@"%ld",(long)cellNumber);
    
    return cell;
}



// TODO: - ödev

// gün listesi bu sınıftad oluşturulacak ama şu şekilde
// public bir liste değil.
// parametre olarak gün index'i girilip NSString göndüren bir fonsiton olarak çalışacak.
// bu fonsiyona gün index'i girilince gün değeri dönecek.
// ve itile oluşturmak için bu fooksiyon kullanılacak.

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
