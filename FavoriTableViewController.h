//
//  FavoriTableViewController.h
//  OrantilaApp
//
//  Created by Emre YILMAZ on 03/11/15.
//  Copyright © 2015 Emre YILMAZ. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface FavoriTableViewController : UITableViewController
-(void)kullaniciSorgula;

-(void) refresh;

@end
