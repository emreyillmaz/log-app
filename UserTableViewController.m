//
//  UserTableViewController.m
//  OrantilaApp
//
//  Created by Emre YILMAZ on 03/11/15.
//  Copyright © 2015 Emre YILMAZ. All rights reserved.
//

#import "UserTableViewController.h"
#import "ETGlobal.h"

@interface UserTableViewController ()

@end

@implementation UserTableViewController

@synthesize uILabelBaslik;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBar.topItem.title = TEXT(@"Title_User");
    
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
   // [self.tableView.section]
    //[self.tableView.numberOfSections];
}
-(void)viewWillAppear:(BOOL)animated{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [[self.tableView cellForRowAtIndexPath:indexPath].textLabel setText:TEXT(@"Title_user")];
    NSLog(@"%@ dsdsd",[[self.tableView cellForRowAtIndexPath:indexPath].textLabel text]);
    NSLog(@"%@ index path ",[[self.tableView cellForRowAtIndexPath:indexPath].textLabel text]);
    uILabelBaslik.text = TEXT(@"User_LogOut");

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//#warning Incomplete implementation, return the number of sections
//    return 0;
//}
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//#warning Incomplete implementation, return the number of rows
//    return 0;
//}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section ==0 && indexPath.row == 0)
    {
        //Do what you want to do.
        NSLog(@"tıklandı..");
       
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:TEXT(@"User_Alert_Uyari")
                                                                       message:@""
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:TEXT(@"OK")
                                                                style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {
                                                                  [PFUser logOut];
                                                                  PFUser *currentUser = [PFUser currentUser];
                                                                  
                                                                  UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                                                  UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"tabBarController"];
                                                                  vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
                                                                  [self presentViewController:vc animated:YES completion:NULL];

                                                              }];
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:TEXT(@"Cancel") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
           
        }];
        
        [alert addAction:ok];
        [alert addAction:cancel];
        [self presentViewController:alert animated:YES completion:nil];
    
    
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *sectionName;
    switch (section)
    {
        case 0:
            sectionName = TEXT(@"User_Account");
            break;
    }
    return sectionName;
}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
